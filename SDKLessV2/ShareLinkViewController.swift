//
//  ShareLinkViewController.swift
//  SDKLessV2
//
//  Created by Jumpei Katayama on 9/24/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import Alamofire

class ShareLinkViewController: BaseViewController {

    @IBOutlet weak var labelShareURL: UILabel!
    
    var shareURL: String = "" {
        didSet {
            labelShareURL.text = shareURL
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageName = "ShareLinkView"

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let endpoint = "https://dev.streethawk.com/v2/events/sharelink"
        
        let params = [
            "utm_campaign": "test_campaign",
            "utm_source": "yahoo",
            "utm_medium": "cpc",
            "utm_content": "text_link",
            "utm_term": "running+",
            "scheme": "less",
            "uri": "home",
            "default_destination_url": "http://google.com"]
        
        guard let installid = NSUserDefaults.standardUserDefaults().objectForKey("installid") as? String else {
            print("no installID found")
            return
        }

        headers = [
            "X-Event-Key": "evtqj2JH1CkHqUGBRoqzJAxwytrG3ZoLf",
            "X-App-Key": "SDKLessV2Swift",
            "X-Installid": installid]
        
        Alamofire.request(.POST, endpoint, parameters: params, encoding: .JSON, headers: headers)
            .responseJSON { request, response, result in
                print(params)
                print(self.headers)
                print(request)
                self.shareURL = result.value! as! String
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func dismiss(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

}
