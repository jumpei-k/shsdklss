//
//  AppDelegate.swift
//  SDKLessV2
//
//  Created by Jumpei Katayama on 9/22/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import Alamofire
import CoreTelephony
import CoreLocation
//JP
private let domain = "https://dev.streethawk.com"
private let appKey = "SDKLessV2Swift"
private let eventKey = "evtqj2JH1CkHqUGBRoqzJAxwytrG3ZoLf"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    //  MARK: - Application State
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        if let options = launchOptions {
            print(options)
        } else {
            print("nothing in lanchoptions")
        }
        
        
        print("didFinishLaunchingWithOptions")

        // Override point for customization after applxication launch.
        if !NSUserDefaults.standardUserDefaults().boolForKey("HasLaunchedOnce") {
            print("--------------------------");
            print("This is first time launch");
            print("--------------------------");
            // Configure remote notification

            let params = [
                "model": "iPhone 6",
                "os_version": "8.1",
                "operating_system": "iOS",
                "width": "750",
                "height": "1334",
                "utc_offset": "-480",
                "share_guid_url": "http://hwk.io/s/Xa6DrV"
            ]            // Parameters for register for deeplinking
            
            
            Alamofire.request(.POST, "https://dev.streethawk.com/v2/events/register", parameters: params, encoding: .JSON, headers: ["X-Event-Key": eventKey, "X-App-Key": appKey])
                .responseJSON { request, response, result in
                    print(request)
                    print(response)
                    print(result)
                    print(result.value!)
                    // Retrieve InstallID
                    let reponseData = result.value!
                    print(reponseData["installid"])
                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "HasLaunchedOnce")

                    NSUserDefaults.standardUserDefaults().setValue(reponseData["installid"]!, forKey: "installid")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    
                    let types: UIUserNotificationType = [.Badge,.Alert,.Sound]
                    let settings: UIUserNotificationSettings = UIUserNotificationSettings( forTypes: types, categories: nil )
                    application.registerUserNotificationSettings(settings)// presents dialog
                    application.registerForRemoteNotifications()// This also should trigger DidfinishRegisterNotifcationSetting
            }
        }
        return true
    }
    

    func applicationWillResignActive(application: UIApplication) {
        print("applicationWillResignActive")

    }

    func applicationDidEnterBackground(application: UIApplication) {
        print("applicationDidEnterBackground")
        
        if let installid = NSUserDefaults.standardUserDefaults().objectForKey("installid") as? String {
            let endpoint = "https://dev.streethawk.com/v2/events/background"
            let headers = ["X-Event-Key": eventKey, "X-App-Key": appKey, "X-Installid": installid]
            print("sending events for background")
            // Update install for notification
            Alamofire.request(.POST, endpoint, parameters: nil, encoding: .JSON, headers: headers)
                .responseJSON { request, response, result in
                    print(result.value!)
                    // Retrieve InstallID
            }
        }
    }
    

    func applicationWillEnterForeground(application: UIApplication) {
        print("applicationWillEnterForeground")
        if let installid = NSUserDefaults.standardUserDefaults().objectForKey("installid") as? String {
            // If the device has install id
            let headers = ["X-Event-Key": eventKey, "X-App-Key": appKey, "X-Installid": installid]
            let endpoint = "https://dev.streethawk.com/v2/events/foreground"
            print("sending events for foreground")
            // Update install for notification
            Alamofire.request(.POST, endpoint, parameters: nil, encoding: .JSON, headers: headers)
                .responseJSON { request, response, result in
                    print(result.value!)
                    // Retrieve InstallID
            }
        }
        
        let delayInSeconds: Double = 3.0
        let poptime: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(delayInSeconds * Double(NSEC_PER_SEC)))
        dispatch_after(poptime, dispatch_get_main_queue(), {
        })

        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        print("applicationDidBecomeActive")
    }

    func applicationWillTerminate(application: UIApplication) {
        print("applicationWillTerminate")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    // - MARK: - Push Delegate
    // ==================================================================
    // Called When the device register notification setting
    // ==================================================================
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        print("didRegisterUserNotificationSettings is called")
    }
    
    
    // ==================================================================
    // Called When the device registerd remote notifications
    // ==================================================================
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        print("didRegisterForRemoteNotificationsWithDeviceToken is called")
        
        let characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
        
        let deviceTokenString: String = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
        
        print("device token is")
        print(deviceTokenString)
        
        func getCarrierName() -> String {
            let netinfo = CTTelephonyNetworkInfo()
            let carrier = netinfo.subscriberCellularProvider
            return (carrier?.carrierName)!
        }
        
        // Update install
        // Retrieve InstallID
        let endpoint = "https://dev.streethawk.com/v2/events/update"
        let params = [
            "access_data": deviceTokenString,
            "carrier_name": getCarrierName(),
            "client_version": "1.1",
            "development_platform": "native",
            "live": "false",
            "operating_system": "ios",
            "os_version": UIDevice.currentDevice().systemVersion,
            "sh_cuid": "\(helper.randomUserIdentifier(Int(arc4random_uniform(UInt32(10 - 7))) + 7))",
            "model": UIDevice.currentDevice().model,
            "mode": "dev",
            "utc_offset": "-420",
        ];
        
        let installid = NSUserDefaults.standardUserDefaults().objectForKey("installid") as! String
        let headers = ["X-Event-Key": eventKey, "X-App-Key": appKey, "X-Installid": installid]
        // Update install for notification
        Alamofire.request(.POST, endpoint, parameters: params, encoding: .JSON, headers: headers)
            .responseJSON { request, response, result in
                print(result.value!)
        }
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        print("didReceiveLocalNotification")
    }
    
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, completionHandler: () -> Void) {
        print("handleActionWithIdentifierforLocalNotification:completionHandler")
    }
    
    func application(application: UIApplication, performFetchWithCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        ////        AppDelegateIn
    }
    
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        print("opening app from URL")
        print(url)
        print(options)
        guard let host = url.host else {
            print("no host name found")
            return true
        }
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var vc: UIViewController
        switch host {
            case "home":
                print("oepning home....")
                vc = storyboard.instantiateViewControllerWithIdentifier("HomeViewController")
                self.window?.rootViewController?.navigationController?.pushViewController(vc, animated: true)
                
                print("")
            
        default: break;
            
        }

        return true
    }
    
    
    
    // Failed to register
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }
    
    
    //Since iOS 7, same as above but able to perform background task. Because this function, above function actually is NEVER called since iOS 7; in iOS 6 above function is called.

    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        let state = application.applicationState
        print("-------")
        print(userInfo)
        print("---------")
        if let aps = userInfo["aps"] as? NSDictionary {
            
            switch state {
            case .Active:
                print("Active")
                print(aps)
                completionHandler(.NewData)
//                application.
                
            case .Inactive:
                print("Inactive")
                let message: NSDictionary = userInfo["aps"] as! NSDictionary
                let alert = message["alert"]
                print("Message: \(alert)")
                completionHandler(.NoData)
            case .Background:
                print("Background")
                let message: NSDictionary = userInfo["aps"] as! NSDictionary
                let alert = message["alert"]
                print("Message: \(alert)")
                completionHandler(.Failed)
            }
        }
    }
    
    //called when notification arrives and:
    //1. App in BG notification bannder show, pull down and click the button can call this.
    //2. NOT called when App in FG.
    //3. NOT called when click notification banner directly.
    //This delegate callback not mixed with above `didReceiveRemoteNotification`.
    
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [NSObject : AnyObject], completionHandler: () -> Void) {
        print(userInfo["c"])
        print(userInfo["i"])
        print(userInfo["d"])
        print(userInfo["p"])
        print(userInfo["o"])
        print(userInfo["s"])
        print(userInfo["l"])
        print(userInfo["n"])
        if let dicAps: NSDictionary = userInfo["aps"] as? NSDictionary {
            
            switch application.applicationState {
            case .Active:
                print("Active")
                print(dicAps["sound"])
                print(dicAps["badge"])
                
            case .Inactive:
                print("Inactive")
                //                let message: NSDictionary = userInfo["aps"] as! NSDictionary
                let alert = dicAps["alert"]
                print("Message: \(alert)")
            case .Background:
                print("Background")
                let alert = dicAps["alert"]
                print("Message: \(alert)")
            }
        }
        print(userInfo.keys)
        //        print(userInfo.allKeys)
        
        print("")
    }


}

