//
//  pageViewEvent.swift
//  SDKLessV2
//
//  Created by Jumpei Katayama on 9/28/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import Foundation

protocol pageViewEvent {
    func enterPageView(pageName: String)
    
}