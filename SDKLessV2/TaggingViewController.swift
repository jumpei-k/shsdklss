//
//  TaggingViewController.swift
//  SDKLessV2
//
//  Created by Jumpei Katayama on 9/24/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import Alamofire

class TaggingViewController: BaseViewController, UITextFieldDelegate {
    
    var viewNeedsAdjustment = false

    // This constraint ties an element at zero points from the bottom layout guide
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var textFiledTagStringName: UITextField! {
        didSet {
            textFiledTagStringName.delegate = self
        }
    }
    @IBOutlet weak var textFiledTagStringValue: UITextField! {
        didSet {
            textFiledTagStringValue.delegate = self
        }
    }
    
    @IBOutlet weak var textFiledTagNumericName: UITextField! {
        didSet {
            textFiledTagNumericName.delegate = self
        }
    }

    @IBOutlet weak var textFiledTagNumericValue: UITextField!  {
        didSet {
            textFiledTagNumericValue.delegate = self
        }
    }

    @IBOutlet weak var textFiledTagDeleteName: UITextField!  {
        didSet {
            textFiledTagDeleteName.delegate = self
        }
    }

    
    var tagStringName: String {
        get {
            return textFiledTagStringName.text!
        }
    }
    
    var tagStringValue: String {
        get {
            return textFiledTagStringValue.text!
        }
    }
    
    var tagNumericName: String {
        get {
            return textFiledTagNumericName.text!
        }
    }
    
    var tagNumericValue: String {
        get {
            return textFiledTagNumericValue.text!
        }
    }
    
    var tagDeleteName: String {
        get {
            return textFiledTagDeleteName.text!
        }
    }
    
    let installid: String = NSUserDefaults.standardUserDefaults().objectForKey("installid") as! String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageName = "TaggingView"
        registeForKeyboardNotifications()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    @IBAction func sendTagNumeric(sender: AnyObject) {
        let params = ["key": tagNumericName, "numeric": tagNumericValue, "increment": "true"]
        
        Alamofire.request(.POST, "https://dev.streethawk.com/v2/events/tag", parameters: params, encoding: .JSON, headers: ["X-Event-Key": "evtqj2JH1CkHqUGBRoqzJAxwytrG3ZoLf", "X-App-Key": "SDKLessV2Swift", "X-Installid": installid])
            .responseJSON { request, response, result in
                print(request)
                print(response)
                print(result)
                print(result.value!)
        }
    }
    
    
    @IBAction func sendTagString(sender: AnyObject) {
        let params = ["key": tagStringName, "string": tagStringValue]
        
        Alamofire.request(.POST, "https://dev.streethawk.com/v2/events/tag", parameters: params, encoding: .JSON, headers: ["X-Event-Key": "evtqj2JH1CkHqUGBRoqzJAxwytrG3ZoLf", "X-App-Key": "SDKLessV2Swift", "X-Installid": installid])
            .responseJSON { request, response, result in
                print(request)
                print(response)
                print(result)
                print(result.value!)
                // Retrieve InstallID
//                let reponseData = result.value!
        }
        
    }
    
    @IBAction func deleteTag(sender: AnyObject) {
        let params = ["key": tagDeleteName, "delete": "true"]
        
        Alamofire.request(.POST, "https://dev.streethawk.com/v2/events/tag", parameters: params, encoding: .JSON, headers: ["X-Event-Key": "evtqj2JH1CkHqUGBRoqzJAxwytrG3ZoLf", "X-App-Key": "SDKLessV2Swift", "X-Installid": installid])
            .responseJSON { request, response, result in
                print(request)
                print(response)
                print(result)
                print(result.value!)
                // Retrieve InstallID
//                let reponseData = result.value!
        }
    }
    
    func registeForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardDidShowNotification, object: nil)

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    func keyboardWasShown(notification: NSNotification) {
        // View will be adjust if the text field lcates below the center of the y
        if viewNeedsAdjustment {
            let info = notification.userInfo!
            let kbframe: CGRect = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
            let duration: NSTimeInterval = (info[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = info[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            self.keyboardHeightLayoutConstraint?.constant = kbframe.size.height ?? 0.0
            
            UIView.animateWithDuration(duration,
                delay: NSTimeInterval(0),
                options: animationCurve,
                animations: {self.view.layoutIfNeeded() },
                completion: nil)
        }

     }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        if viewNeedsAdjustment {
            let info = notification.userInfo!
            //        let kbframe: CGRect = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
            let duration: NSTimeInterval = (info[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = info[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            self.keyboardHeightLayoutConstraint?.constant = 0.0
            
            UIView.animateWithDuration(duration,
                delay: NSTimeInterval(0),
                options: animationCurve,
                animations: {self.view.layoutIfNeeded()},
                completion: nil)
        }
        
    }
    // Called when the UIKeyboardDidShowNotification is sent.

    
    //  MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        print("\(textField.frame) is selecrted")
        if textField.frame.origin.y > view.center.y {
            viewNeedsAdjustment = true
        } else {
            viewNeedsAdjustment = false
        }
        print(viewNeedsAdjustment)
        return true
    }
    

}
