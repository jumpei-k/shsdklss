//
//  ViewController.swift
//  SDKLessV2
//
//  Created by Jumpei Katayama on 9/22/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

private let appKey = "SDKLessV2Swift"
private let eventKey = "evtqj2JH1CkHqUGBRoqzJAxwytrG3ZoLf"

class ViewController: BaseViewController, CLLocationManagerDelegate {

    var locationManager: CLLocationManager?
    var fgMinTimeBetweenEvents: NSTimeInterval = 60
    var bgMinTimeBetweenEvents: NSTimeInterval! = 300
    var fgMinDistanceBetweenEvents: Float = 100.0
    var bgMinDistanceBetweenEvents: Float = 500.0
    var currentGeoLocationValue = CLLocationCoordinate2DMake(0, 0)
    var sentGeoLocationValue = CLLocationCoordinate2DMake(0, 0)
    var sentGeoLocationTime = 0
//    var installid: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageName = "View"
//        installid = NSUserDefaults.standardUserDefaults().objectForKey("installid") as! String

        createLocationManager()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateLocationSuccessNotificationHandler:", name: "SHLMUpdateLocationSuccessNotification", object: nil)
        locationManager?.startUpdatingLocation()
      // Do any additional setup after loading the view, typically from a nib.
    }
    
    func createLocationManager() {
        print("create corelocation manager")
        locationManager = CLLocationManager()
        locationManager!.delegate = self
        locationManager!.pausesLocationUpdatesAutomatically = false
        locationManager!.distanceFilter = 100
        requestPermission()
        //initialize detecting location
        // Start
    }
    
    
    func requestPermission() {
        self.locationManager!.requestAlwaysAuthorization()
        self.locationManager!.requestWhenInUseAuthorization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func deleteInstallInformation(sender: AnyObject) {
    
        
        if let installid = NSUserDefaults.standardUserDefaults().objectForKey("installid") as? String {
            let endpoint = "https://dev.streethawk.com/v2/events/delete"
//            let installid = NSUserDefaults.standardUserDefaults().objectForKey("installid") as! String
            let params = ["delete": "true"]
            let headers = ["X-Event-Key": eventKey, "X-App-Key": appKey, "X-Installid": installid]
            print("sending events for delete")
            // Update install for notification
            Alamofire.request(.POST, endpoint, parameters: params, encoding: .JSON, headers: headers)
                .responseJSON { request, response, result in
                    print(result.value!)
                    // Retrieve InstallID
            }
        }

    }
    
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var authStatus: String
        switch status {
        case .AuthorizedAlways:
            authStatus = "always"
        case .AuthorizedWhenInUse:
            authStatus = "when in use"
        case .Denied:
            authStatus = "denied"
        case .NotDetermined:
            authStatus = "not determined"
        case .Restricted:
            authStatus = "restricted"
        }
        
        print("LocationManager Delegate: Authorisation status changed: \(authStatus)")
        
    }
    
    func currentGeoLocation() -> CLLocationCoordinate2D {
        return currentGeoLocationValue
    }
    
    func sendGeoLocationUpdate() {
        if currentGeoLocation().latitude == 0 || currentGeoLocation().longitude == 0 {
            return
        }
        
        if let installid = NSUserDefaults.standardUserDefaults().objectForKey("installid") as? String {
            let params = ["latitude": "\(currentGeoLocation().latitude)", "longitude": "\(currentGeoLocation().longitude)"]
            print("sending events for locations")
            Alamofire.request(.POST, "https://dev.streethawk.com/v2/events/locations", parameters: params, encoding: .JSON, headers: ["X-Event-Key": "evtqj2JH1CkHqUGBRoqzJAxwytrG3ZoLf", "X-App-Key": "SDKLessV2Swift", "X-Installid": installid])
                .responseJSON { request, response, result in
                    print(request)
                    print(response)
                    print(result)
                    print(result.value!)
                    // Retrieve InstallID
                    let reponseData = result.value!
            }
        }
    }
    
    
    
    // MARK: - Location notification console log
    
    func updateLocationSuccessNotificationHandler(notification: NSNotification) {
        let userinfo = notification.userInfo as! [String: AnyObject]
        let newLocation = userinfo["NewLocation"] as! CLLocation
        let oldLocation = userinfo["OldLocation"] as! CLLocation
        print("Update success from \(oldLocation.coordinate.latitude): \(oldLocation.coordinate.longitude) to \(newLocation.coordinate.latitude): \(newLocation.coordinate.longitude)")
        
    }
    
    
    func locationManager(manager: CLLocationManager, didDetermineState state: CLRegionState, forRegion region: CLRegion) {
        
    }
    
    func locationManager(manager: CLLocationManager, didFinishDeferredUpdatesWithError error: NSError?) {
        print(error)
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print(error)
    }
    func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("didExitRegion")
    }
    
    func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("didEnterRegion")
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        print("didUpdateToLocation")
    }
    
    func locationManager(manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        
    }
    
    func locationManager(manager: CLLocationManager, didStartMonitoringForRegion region: CLRegion) {
        print("didStartMonitoringForRegion")
    }
    
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        print("didUpdateHeading")
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didUpdateLocations")
        if locations.count > 0 {
            let previousLocation: CLLocationCoordinate2D = currentGeoLocation()
            currentGeoLocationValue = locations[0].coordinate
            sendGeoLocationUpdate()
            let oldLocation = CLLocation(latitude: previousLocation.latitude, longitude: previousLocation.longitude)
            let userInfo = ["NewLocation": locations[0], "OldLocation": oldLocation]
            let notification: NSNotification = NSNotification(name: "SHLMUpdateLocationSuccessNotification", object: self, userInfo: userInfo)
            NSNotificationCenter.defaultCenter().postNotification(notification)
        }
    }
    
    func locationManager(manager: CLLocationManager, didVisit visit: CLVisit) {
        print("didVisit")
    }
    
    func locationManager(manager: CLLocationManager, monitoringDidFailForRegion region: CLRegion?, withError error: NSError) {
        print("monitoringDidFailForRegion")
    }
    
    func locationManager(manager: CLLocationManager, rangingBeaconsDidFailForRegion region: CLBeaconRegion, withError error: NSError) {
        print("rangingBeaconsDidFailForRegion")
    }
    
    func locationManagerDidPauseLocationUpdates(manager: CLLocationManager) {
        print("locationManagerDidPauseLocationUpdates")
    }
    
    func locationManagerDidResumeLocationUpdates(manager: CLLocationManager) {
        print("locationManagerDidResumeLocationUpdates")
    }
    
    func locationManagerShouldDisplayHeadingCalibration(manager: CLLocationManager) -> Bool {
        return true
    }
}


