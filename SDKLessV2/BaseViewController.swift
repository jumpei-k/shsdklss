//
//  BaseViewController.swift
//  SDKLessV2
//
//  Created by Jumpei Katayama on 9/24/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import Alamofire


class BaseViewController: UIViewController {

    private let endpointEnter: String = "https://dev.streethawk.com/v2/events/enter"
    private let endpointExit: String = "https://dev.streethawk.com/v2/events/exit"

    var headers: [String: String]!
    var pageName: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let params = ["page": pageName]
        print("Entered \(self.pageName)")

        if let installid = NSUserDefaults.standardUserDefaults().objectForKey("installid") as? String {
            headers = [
                "X-Event-Key": "evtqj2JH1CkHqUGBRoqzJAxwytrG3ZoLf",
                "X-App-Key": "SDKLessV2Swift",
                "X-Installid": installid,
            ]
            Alamofire.request(.POST, endpointEnter, parameters: params, encoding: .JSON, headers: headers)
                .responseJSON { request, response, result in
                    print(params)
                    print(self.headers)
                    print(request)
                    print(result.value!)
            }
        }

    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        print("Exited \(pageName)")
        let params = ["page": pageName]
        if let installid = NSUserDefaults.standardUserDefaults().objectForKey("installid") as? String {
            headers = [
                "X-Event-Key": "evtqj2JH1CkHqUGBRoqzJAxwytrG3ZoLf",
                "X-App-Key": "SDKLessV2Swift",
                "X-Installid": installid,
            ]
            Alamofire.request(.POST, endpointEnter, parameters: params, encoding: .JSON, headers: headers)
                .responseJSON { request, response, result in
                    print(params)
                    print(self.headers)
                    print(request)
                    print(result.value!)
            }
        }
    }

}
