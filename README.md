# SDKLessV2
New StreetHawk API with Swift 2.0

## Usage
### Install dependencies
``` shell
$ pod install
```


### Enviroment
- Swift 2.0
- Xcode 7

### Dependencies
- Alamofire 2.0

## TODO
- [x] Register install
- [x] update installs
- [x] Generate a sharelink
- [ ] Register with deep link works
- [x] Send session events
- [x] Send PageView Events
- [x] Send locationu update events
- [x] Send and receive push notifications
- [x] Send Heartbeat (hard coding)
- [x] Send tagging string, numeric, delete
- [x] Delete install
- [ ] Implement push message resul
